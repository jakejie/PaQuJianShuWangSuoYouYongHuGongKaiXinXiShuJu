# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JianshuItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    #用户名
    name = scrapy.Field()
    #主页链接地址
    users_url = scrapy.Field()
    #关注量
    attention = scrapy.Field()
    #粉丝数
    fans = scrapy.Field()
    #文章数
    article = scrapy.Field()
	#字数
    words_num = scrapy.Field()
    #收获喜欢
    gain_like = scrapy.Field()
    #收获喜欢
    intro  = scrapy.Field()
